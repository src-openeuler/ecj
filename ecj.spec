Epoch:          1

%global qualifier R-4.12-201906051800

Name:           ecj
Version:        4.12
Release:        1
Summary:        the Eclipse Compiler for Java
License:        EPL-2.0
URL:            http://www.eclipse.org

Source0:        http://download.eclipse.org/eclipse/downloads/drops4/%{qualifier}/ecjsrc-%{version}.jar
Source1:        https://repo1.maven.org/maven2/org/eclipse/jdt/ecj/3.18.0/ecj-3.18.0.pom
Source2:        MANIFEST.MF
Source3:        java10api.jar

Patch0: %{name}-rpmdebuginfo.patch
Patch1: javaAPI.patch

BuildArch:      noarch
BuildRequires:  ant javapackages-local

%description
ECJ is the the Eclipse Compiler for Java.  It is also known as the JDT Core batch compiler.

%package_help

%prep
%autosetup -c -n %{name}-%{version} -p1

sed -i -e 's|debuglevel=\"lines,source\"|debug=\"yes\"|g' build.xml

cp %{SOURCE1} pom.xml
mkdir -p scripts/binary/META-INF/
cp %{SOURCE2} scripts/binary/META-INF/MANIFEST.MF

rm -f org/eclipse/jdt/core/JDTCompilerAdapter.java

%mvn_alias org.eclipse.jdt:ecj org.eclipse.jdt:core org.eclipse.jdt.core.compiler:ecj \
  org.eclipse.tycho:org.eclipse.jdt.core org.eclipse.tycho:org.eclipse.jdt.compiler.apt
%mvn_artifact "org.eclipse:java10api:jar:10" %{SOURCE3}
%mvn_alias "org.eclipse:java10api:jar:10" "org.eclipse:java9api:jar:9"

%build
ant -Djavaapi=%{SOURCE3}

%install
%mvn_artifact pom.xml ecj.jar
%mvn_install

%jpackage_script org.eclipse.jdt.internal.compiler.batch.Main '' '' ecj ecj true
mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 -p ecj.1 %{buildroot}%{_mandir}/man1/ecj.1

%files -f .mfiles
%defattr(-,root,root)
%doc about.html
%{_bindir}/ecj

%files help
%defattr(-,root,root)
%{_mandir}/man1/ecj*

%changelog
* Mon May 23 2022 chenchen <chen_aka_jan@163.com> - 1:4.12-1
- downgrade to 4.12

* Wed May 18 2022 chenchen <chen_aka_jan@163.com> -  1:4.23-1
- update to 4.23

* Wed Dec 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:4.9-3
- Package init
